
public class Epoca {
	private int idEpoca;
	private String designacao;
	private String dataInicio;
	private String dataFim;
	
	public Epoca(int idepoca, String designacao, String datainicio, String datafim) {
		super();
		this.idEpoca = idepoca;
		this.designacao = designacao;
		this.dataInicio = datainicio;
		this.dataFim = datafim;
	}
	public int getIdepoca() {
		return idEpoca;
	}
	public void setIdepoca(int idepoca) {
		this.idEpoca = idepoca;
	}
	public String getDesignacao() {
		return designacao;
	}
	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}
	public String getDatainicio() {
		return dataInicio;
	}
	public void setDatainicio(String datainicio) {
		this.dataInicio = datainicio;
	}
	public String getDatafim() {
		return dataFim;
	}
	public void setDatafim(String datafim) {
		this.dataFim = datafim;
	}
	
	
}
