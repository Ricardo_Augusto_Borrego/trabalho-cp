
public class Equipa {
	private int idEquipa;
	private String nomeEquipa;
	private String nomeEstadio;
	private String dataFundacao;
	
	
	public Equipa(int idEquipa, String nomeEquipa, String nomeEstadio, String dataFundacao) {
		super();
		this.idEquipa = idEquipa;
		this.nomeEquipa = nomeEquipa;
		this.nomeEstadio = nomeEstadio;
		this.dataFundacao = dataFundacao;
	}
	public int getIdEquipa() {
		return idEquipa;
	}
	public void setIdEquipa(int idEquipa) {
		this.idEquipa = idEquipa;
	}
	public String getNomeEquipa() {
		return nomeEquipa;
	}
	public void setNomeEquipa(String nomeEquipa) {
		this.nomeEquipa = nomeEquipa;
	}
	public String getNomeEstadio() {
		return nomeEstadio;
	}
	public void setNomeEstadio(String nomeEstadio) {
		this.nomeEstadio = nomeEstadio;
	}
	public String getDataFundacao() {
		return dataFundacao;
	}
	public void setDataFundacao(String dataFundacao) {
		this.dataFundacao = dataFundacao;
	}
	
	
	
}
