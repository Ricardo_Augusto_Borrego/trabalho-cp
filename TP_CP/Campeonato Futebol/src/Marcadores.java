
public class Marcadores {
	private int idJogo;
	private int idJogador;
	private int minuto;
	private char autologo;
	
	public Marcadores(int idJogo, int idJogador, int minuto, char autologo) {
		super();
		this.idJogo = idJogo;
		this.idJogador = idJogador;
		this.minuto = minuto;
		this.autologo = autologo;
	}

	public int getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(int idJogo) {
		this.idJogo = idJogo;
	}

	public int getIdJogador() {
		return idJogador;
	}

	public void setIdJogador(int idJogador) {
		this.idJogador = idJogador;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

	public char getAutologo() {
		return autologo;
	}

	public void setAutologo(char autologo) {
		this.autologo = autologo;
	}
	
	
}
