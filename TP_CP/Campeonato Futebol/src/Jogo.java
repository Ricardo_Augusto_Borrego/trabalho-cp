
public class Jogo {
	private int idJogo;
	private int idEquipaCasa;
	private int EquipaVisitante;
	private String data;
	private int idEpoca;
	private int numGolosCasa;
	private int numGolosVisitante;
	
	public Jogo(int idJogo, int idEquipaCasa, int equipaVisitante, String data, int idEpoca, int numGolosCasa,
			int numGolosVisitante) {
		super();
		this.idJogo = idJogo;
		this.idEquipaCasa = idEquipaCasa;
		EquipaVisitante = equipaVisitante;
		this.data = data;
		this.idEpoca = idEpoca;
		this.numGolosCasa = numGolosCasa;
		this.numGolosVisitante = numGolosVisitante;
	}
	
	public int getIdJogo() {
		return idJogo;
	}
	public void setIdJogo(int idJogo) {
		this.idJogo = idJogo;
	}
	public int getIdEquipaCasa() {
		return idEquipaCasa;
	}
	public void setIdEquipaCasa(int idEquipaCasa) {
		this.idEquipaCasa = idEquipaCasa;
	}
	public int getEquipaVisitante() {
		return EquipaVisitante;
	}
	public void setEquipaVisitante(int equipaVisitante) {
		EquipaVisitante = equipaVisitante;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getIdEpoca() {
		return idEpoca;
	}
	public void setIdEpoca(int idEpoca) {
		this.idEpoca = idEpoca;
	}
	public int getNumGolosCasa() {
		return numGolosCasa;
	}
	public void setNumGolosCasa(int numGolosCasa) {
		this.numGolosCasa = numGolosCasa;
	}
	public int getNumGolosVisitante() {
		return numGolosVisitante;
	}
	public void setNumGolosVisitante(int numGolosVisitante) {
		this.numGolosVisitante = numGolosVisitante;
	}
	
	
}
