
public class Plantel {
	private int idJogador;
	private int idEquipa;
	private int idEpoca;
	
	public Plantel(int idJogador, int idEquipa, int idEpoca) {
		super();
		this.idJogador = idJogador;
		this.idEquipa = idEquipa;
		this.idEpoca = idEpoca;
	}

	public int getIdJogador() {
		return idJogador;
	}

	public void setIdJogador(int idJogador) {
		this.idJogador = idJogador;
	}

	public int getIdEquipa() {
		return idEquipa;
	}

	public void setIdEquipa(int idEquipa) {
		this.idEquipa = idEquipa;
	}

	public int getIdEpoca() {
		return idEpoca;
	}

	public void setIdEpoca(int idEpoca) {
		this.idEpoca = idEpoca;
	}
	
	
}
