
public class Jogador {
	private int idJogador;
	private String nomeJogador;
	private String dataNascimento;
	private String posicaoHabitual;
	
	public Jogador(int idJogador, String nomeJogador, String dataNascimento, String posicaoHabitual) {
		super();
		this.idJogador = idJogador;
		this.nomeJogador = nomeJogador;
		this.dataNascimento = dataNascimento;
		this.posicaoHabitual = posicaoHabitual;
	}

	public int getIdJogador() {
		return idJogador;
	}

	public void setIdJogador(int idJogador) {
		this.idJogador = idJogador;
	}

	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getPosicaoHabitual() {
		return posicaoHabitual;
	}

	public void setPosicaoHabitual(String posicaoHabitual) {
		this.posicaoHabitual = posicaoHabitual;
	}
	
	
	
}
